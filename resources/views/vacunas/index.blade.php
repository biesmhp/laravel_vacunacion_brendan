@extends('layouts.master')
@section('title')
    Vacunas
@endsection
@section('contenido')
    <div class="row">
        @foreach( $vacunas as $clave => $vacuna)
        <div class="col-xs-12col-sm-6col-md-4">
            <a href="{{ route('vacuna.show' , $vacuna[$clave]->slug) }}">
                <h4 style="min-height:45px;margin:5px 0 10px 0">
                    {{$vacuna->nombre}}
                </h4>
            </a>
        </div>
        @endforeach
    </div>
@endsection