<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Vacuna;

class VacunaController extends Controller
{
    //
    public function index()
    {
		$vacunas=DB::table('vacunas')->get();
        return view('vacunas.index',['vacunas'=>$vacunas]);
    }

    public function show(Vacuna $vacuna)
    {
        return view('vacunas.show', ['vacuna'=>$vacuna]);
    }
}
