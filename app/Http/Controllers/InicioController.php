<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class InicioController extends Controller
{
    //
    public function inicio()
    {
        //return view('home');
        return redirect()->action([VacunaController::class,'index']);
    }
}
